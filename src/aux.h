
#ifndef AUX_MBBRL
#define AUX_MBBRL

#define ONLINE 1
#define OFFLINE 0

#include <stdio.h>
#ifdef MSDOS
#include "npp.h"
#else
#include "n++.h"
#endif
#include "PatternSet.h"

namespace brll
{

struct result_type{
  float tss;             
  int   hamdis;    /* hamming distance: number of wrong classified outputs */
};

struct param_type{
  float tolerance;     /* error tolerance for classification error */
  int update_mode;     
}; 

extern param_type aux_params;

void aux_trainAll(Net *net,PatternSet *pat,result_type *result);
void aux_testAll(Net *net,PatternSet *pat,result_type *result);
void aux_testAllVerbose(Net *net,PatternSet *pat,result_type *result);

}  // namespace mbbrl

#endif //AUX_MBBRL
